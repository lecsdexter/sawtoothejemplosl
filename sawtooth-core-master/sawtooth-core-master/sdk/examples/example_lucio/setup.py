from __future__ import print_function

import os
import subprocess

from setuptools import setup, find_packages

print ("Archivo setup.py en ejecucion")

data_files = [] # L: array de listas (path OS -- destino, paths files proyecto -- fuente)

if os.path.exists("/etc/default"):
    data_files.append(
        ('/etc/default', ['packaging/systemd/sawtooth-marina-tp-python']))

if os.path.exists("/lib/systemd/system"):
    data_files.append(('/lib/systemd/system',
                       ['packaging/systemd/sawtooth-marina-tp-python.service']))     


setup(
    name='sawtooth-marinaL',
    version=subprocess.check_output(
        ['../../../bin/get_version']).decode('utf-8').strip(),
    description='Sawtooth Marina Ejemplo',
    author='Plenumsoft Marina',
    packages=find_packages(),
    install_requires=[
        'aiohttp',
        'colorlog',
        'protobuf',
        'sawtooth-sdk',
        'sawtooth-signing',
        'PyYAML',
    ],
    data_files=data_files,
    entry_points={
        'console_scripts': [ ##L: extra features del proyecto-bch; client shell -> marina + command (ej: marina create);
            'marina = sawtooth_marina.marina_cli:main_wrapper', ###
            'marina-tp-python = sawtooth_marina.processor.main:main', ###
        ]
    })



print ("Archivo setup.py ejecutado")