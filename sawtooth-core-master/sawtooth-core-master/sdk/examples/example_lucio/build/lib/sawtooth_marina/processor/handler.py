import logging

from sawtooth_sdk.processor.handler import TransactionHandler
from sawtooth_sdk.processor.exceptions import InvalidTransaction
from sawtooth_sdk.processor.exceptions import InternalError

from sawtooth_marina.processor.marina_payload import MarinaPayload
from sawtooth_marina.processor.marina_state import Trazabilidad
from sawtooth_marina.processor.marina_state import MarinaState
from sawtooth_marina.processor.marina_state import MARINA_NAMESPACE

LOGGER = logging.getLogger(__name__)

class MarinaTransactionHandler(TransactionHandler):

    @property
    def family_name(self):
        return 'marina'

    @property
    def family_versions(self):
        return ['1.0']

    @property
    def namespaces(self):
        return [MARINA_NAMESPACE]

    #combinaremos las ordenes que vienen en la transaccion (marina payload) 
    # y las funciones que cumplen esas ordenes (marina_state).
    def apply(self, transaction, context):

        header = transaction.header
        signer = header.signer_public_key

        marina_payload = MarinaPayload.from_bytes(transaction.payload)
        _display("------------------------------------------------")
        _display("apply: marina_payload.action - {}".format(marina_payload.action))
        _display("------------------------------------------------")

        marina_state = MarinaState(context)

        if marina_payload.action == 'delete':

            trazabilidad = marina_state.get_trazabilidad(marina_payload.folderName)

            if trazabilidad is None:
                raise InvalidTransaction(
                    'Invalid action: trazabilidad does not exist')

            marina_state.borrar_trazabilidad(marina_payload.folderName)

        elif marina_payload.action == 'create': 
            trz_folder = marina_state.get_trazabilidad(marina_payload.folderName)
            #LOGGER.debug("apply - marina_state: " + str(marina_state.get_trazabilidad))            
            #LOGGER.debug("apply - marina_state.get_trazabilidad(marina_payload.folderName): " + str(trz_folder))
            if trz_folder is not None:
                raise InvalidTransaction(
                    'Invalid action: Trazabilidad folder already exists: {}'.format(
                        marina_payload.folderName))

            trz_reg = Trazabilidad(
                        nombreDeLaCarpeta=marina_payload.folderName,
                        pesqueria=marina_payload.fishery,
                        arteDePesca=marina_payload.fishingGear,
                        kg=marina_payload.kg,
                        presentacion=marina_payload.presentation,
                        zonaDeCaptura=marina_payload.captureZone,
                        fecha=marina_payload.date,
                        embarcacion=marina_payload.embarkation
                        )

            marina_state.set_trazabilidad(marina_payload.folderName, trz_reg)
            _display("apply: {} created a trazabilidad folder.".format(signer[:6]))

        elif marina_payload.action == 'update':
            folder = marina_state.get_trazabilidad(marina_payload.folderName)

            if folder is None:
                raise InvalidTransaction(
                    'Invalid action: update requires an existing folder')

            folder.pesqueria = marina_payload.fishery
            folder.arteDePesca = marina_payload.fishingGear
            folder.kg = marina_payload.kg
            folder.presentacion = marina_payload.presentation
            folder.zonaDeCaptura = marina_payload.captureZone
            folder.fecha = marina_payload.date
            folder.embarcacion = marina_payload.embarkation

            marina_state.set_trazabilidad(marina_payload.folderName, folder)
            _display(
                "Datos actualizados por {}: \n\n".format(
                    signer[:6])
                + _folder_data_to_str(
                    folder.pesqueria,
                    folder.arteDePesca,
                    folder.kg,
                    folder.presentacion,
                    folder.zonaDeCaptura,
                    folder.fecha,
                    folder.embarcacion,                    
                    marina_payload.folderName))

        else:
            raise InvalidTransaction('Unhandled action: {}'.format(
                marina_payload.action))

def _folder_data_to_str(pesq,arte,kg,pres,zona,fecha,emb,fname):
    out = ""
    out += "FOLDER: {}\n".format(fname)
    out += "\n"
    out += "PESQUERIA: {}\n".format(pesq)
    out += "ARTE DE PESCA: {}\n".format(arte)
    out += "KG: {}\n".format(kg)
    out += "PRESENTACION: {}\n".format(pres)
    out += "ZONA DE CAPTURA: {}\n".format(zona)
    out += "FECHA: {}\n".format(fecha)
    out += "EMBARCACION: {}\n".format(emb)

    return out

def _display(msg):
    n = msg.count("\n")

    if n > 0:
        msg = msg.split("\n")
        length = max(len(line) for line in msg)
    else:
        length = len(msg)
        msg = [msg]

    # pylint: disable=logging-not-lazy
    LOGGER.debug("+" + (length + 2) * "-" + "+")
    for line in msg:
        LOGGER.debug("+ " + line.center(length) + " +")
    LOGGER.debug("+" + (length + 2) * "-" + "+")