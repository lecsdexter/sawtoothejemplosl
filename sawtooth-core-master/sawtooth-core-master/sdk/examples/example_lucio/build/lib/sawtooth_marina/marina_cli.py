from __future__ import print_function

import argparse
import getpass
import logging
import os
import traceback
import sys
import pkg_resources

from colorlog import ColoredFormatter

from sawtooth_marina.marina_client import MarinaClient
from sawtooth_marina.marina_exceptions import MarinaException

DISTRIBUTION_NAME = 'sawtooth-marina-distribution'

DEFAULT_URL = 'http://127.0.0.1:8008'


def create_console_handler(verbose_level):
    clog = logging.StreamHandler()
    formatter = ColoredFormatter(
        "%(log_color)s[%(asctime)s %(levelname)-8s%(module)s]%(reset)s "
        "%(white)s%(message)s",
        datefmt="%H:%M:%S",
        reset=True,
        log_colors={
            'DEBUG': 'cyan',
            'INFO': 'green',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red',
        })

    clog.setFormatter(formatter)

    if verbose_level == 0:
        clog.setLevel(logging.WARN)
    elif verbose_level == 1:
        clog.setLevel(logging.INFO)
    else:
        clog.setLevel(logging.DEBUG)

    return clog


def setup_loggers(verbose_level):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(create_console_handler(verbose_level))


def add_create_parser(subparsers, parent_parser):
    parser = subparsers.add_parser(
        'create',
        help='Creates a new trazabilidad folder',
        description='Sends a transaction to create a trazabilidad folder with the '
        'identifier <name>. This transaction will fail if the specified '
        'folder already exists.',
        parents=[parent_parser])

    parser.add_argument(
        'name',
        type=str,
        help='identifier for the folder')

    parser.add_argument(
        'pesqueria',
        type=str,
        help='name of the pesqueria')

    parser.add_argument(
        'arteDePesca',
        type=str,
        help='name of the fishing gear') 

    parser.add_argument(
        'kg',
        type=str,
        help='kg')

    parser.add_argument(
        'presentacion',
        type=str,
        help='presentacion')

    parser.add_argument(
        'zonaDeCaptura',
        type=str,
        help='zona de captura')

    parser.add_argument(
        'fecha',
        type=str,
        help='fecha')

    parser.add_argument(
        'embarcacion',
        type=str,
        help='embarcacion')          


    parser.add_argument(
        '--url',
        type=str,
        help='specify URL of REST API')

    parser.add_argument(
        '--username',
        type=str,
        help="identify name of user's private key file")

    parser.add_argument(
        '--key-dir',
        type=str,
        help="identify directory of user's private key file")

    parser.add_argument(
        '--auth-user',
        type=str,
        help='specify username for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--auth-password',
        type=str,
        help='specify password for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--disable-client-validation',
        action='store_true',
        default=False,
        help='disable client validation')

    parser.add_argument(
        '--wait',
        nargs='?',
        const=sys.maxsize,
        type=int,
        help='set time, in seconds, to wait for folder to commit')


def add_list_parser(subparsers, parent_parser):
    parser = subparsers.add_parser(
        'list',
        help='Displays information for all trazabilidad folders',
        description='Displays information for all trazabilidad folders in state, showing '
        'the pesqueria, the arte de pesca, kg, presentacion, zona de captura, fecha, embarcacion and the autor del registro for each folder.',
        parents=[parent_parser])

    parser.add_argument(
        '--url',
        type=str,
        help='specify URL of REST API')

    parser.add_argument(
        '--username',
        type=str,
        help="identify name of user's private key file")

    parser.add_argument(
        '--key-dir',
        type=str,
        help="identify directory of user's private key file")

    parser.add_argument(
        '--auth-user',
        type=str,
        help='specify username for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--auth-password',
        type=str,
        help='specify password for authentication if REST API '
        'is using Basic Auth')


def add_show_parser(subparsers, parent_parser):
    parser = subparsers.add_parser(
        'show',
        help='Displays information about a trazabilidad folder',
        description='Displays the trazabilidad folder <name>, showing '
        'the pesqueria, the arte de pesca, kg, presentacion, zona de captura, fecha, embarcacion and the autor del registro',
        parents=[parent_parser])

    parser.add_argument(
        'name',
        type=str,
        help='identifier for the folder')

    parser.add_argument(
        '--url',
        type=str,
        help='specify URL of REST API')

    parser.add_argument(
        '--username',
        type=str,
        help="identify name of user's private key file")

    parser.add_argument(
        '--key-dir',
        type=str,
        help="identify directory of user's private key file")

    parser.add_argument(
        '--auth-user',
        type=str,
        help='specify username for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--auth-password',
        type=str,
        help='specify password for authentication if REST API '
        'is using Basic Auth')


def add_insert_trz_parser(subparsers, parent_parser):
    parser = subparsers.add_parser(
        'update',
        help='Insert trazilidad data in an trazabilidad folder',
        description='Sends a transaction to insert info of trazability in the trazabilidad folder '
        'with the identifier <name>. This transaction will fail if the '
        'specified folder does not exist.',
        parents=[parent_parser])

    parser.add_argument(
        'name',
        type=str,
        help='identifier for the folder')

    parser.add_argument(
        'pesqueria',
        type=str,
        help='name of the pesqueria')

    parser.add_argument(
        'arteDePesca',
        type=str,
        help='name of the fishing gear') 

    parser.add_argument(
        'kg',
        type=str,
        help='kg')

    parser.add_argument(
        'presentacion',
        type=str,
        help='presentacion')

    parser.add_argument(
        'zonaDeCaptura',
        type=str,
        help='zona de captura')

    parser.add_argument(
        'fecha',
        type=str,
        help='fecha')

    parser.add_argument(
        'embarcacion',
        type=str,
        help='embarcacion')          



    parser.add_argument(
        '--url',
        type=str,
        help='specify URL of REST API')

    parser.add_argument(
        '--username',
        type=str,
        help="identify name of user's private key file")

    parser.add_argument(
        '--key-dir',
        type=str,
        help="identify directory of user's private key file")

    parser.add_argument(
        '--auth-user',
        type=str,
        help='specify username for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--auth-password',
        type=str,
        help='specify password for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--wait',
        nargs='?',
        const=sys.maxsize,
        type=int,
        help='set time, in seconds, to wait for take transaction '
        'to commit')


def add_delete_parser(subparsers, parent_parser):
    parser = subparsers.add_parser('delete', parents=[parent_parser])

    parser.add_argument(
        'name',
        type=str,
        help='name of the folder to be deleted')

    parser.add_argument(
        '--url',
        type=str,
        help='specify URL of REST API')

    parser.add_argument(
        '--username',
        type=str,
        help="identify name of user's private key file")

    parser.add_argument(
        '--key-dir',
        type=str,
        help="identify directory of user's private key file")

    parser.add_argument(
        '--auth-user',
        type=str,
        help='specify username for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--auth-password',
        type=str,
        help='specify password for authentication if REST API '
        'is using Basic Auth')

    parser.add_argument(
        '--wait',
        nargs='?',
        const=sys.maxsize,
        type=int,
        help='set time, in seconds, to wait for delete transaction to commit')


def create_parent_parser(prog_name):
    parent_parser = argparse.ArgumentParser(prog=prog_name, add_help=False)
    parent_parser.add_argument(
        '-v', '--verbose',
        action='count',
        help='enable more verbose output')

    try:
        version = pkg_resources.get_distribution(DISTRIBUTION_NAME).version
    except pkg_resources.DistributionNotFound:
        version = 'UNKNOWN'

    parent_parser.add_argument(
        '-V', '--version',
        action='version',
        version=(DISTRIBUTION_NAME + ' (Hyperledger Sawtooth) version {}')
        .format(version),
        help='display version information')

    return parent_parser


def create_parser(prog_name):
    parent_parser = create_parent_parser(prog_name)

    parser = argparse.ArgumentParser(
        description='Provides subcommands to record maarine trazability  '
        'by sending Marina transactions.',
        parents=[parent_parser])

    subparsers = parser.add_subparsers(title='subcommands', dest='command')

    subparsers.required = True

    add_create_parser(subparsers, parent_parser)
    add_list_parser(subparsers, parent_parser)
    add_show_parser(subparsers, parent_parser)
    add_insert_trz_parser(subparsers, parent_parser)
    add_delete_parser(subparsers, parent_parser)

    return parser


def do_list(args):
    url = _get_url(args)
    auth_user, auth_password = _get_auth_info(args)

    client = MarinaClient(base_url=url, keyfile=None)

    folder_list = [
        folder.split(',')
        for folders in client.list(auth_user=auth_user,
                                 auth_password=auth_password)
        for folder in folders.decode().split('|')
    ]

    if folder_list is not None:
        #print("do_list: " + str(folder_list))
        fmt = "%s %s %s %s %s %s %s %s"
        print(fmt % ('CARPETA', 'PESQUERIA', 'ARTE DE PESCA', 'KG', 'PRESENTACION', 'ZONA DE CAPTURA', 'FECHA', 'EMBARCACION'))
        for folder_data in folder_list:

            nombreDeLaCarpeta, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion = folder_data

            print(fmt % (nombreDeLaCarpeta, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion))
    else:
        raise MarinaException("Could not retrieve folder listing.")


def do_show(args):
    name = args.name

    url = _get_url(args)
    auth_user, auth_password = _get_auth_info(args)

    client = MarinaClient(base_url=url, keyfile=None)

    data = client.show(name, auth_user=auth_user, auth_password=auth_password)

    #print(data.decode().split('|'))

    if data is not None:

        foldername, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion = {
            name: (foldername_, pesqueria_, arteDePesca_, kg_, presentacion_, zonaDeCaptura_, fecha_, embarcacion_)
            for foldername_, pesqueria_, arteDePesca_, kg_, presentacion_, zonaDeCaptura_, fecha_, embarcacion_ in [
                folder.split(',')
                for folder in data.decode().split('|')
            ]
        }[name]


        print("CARPETA:     : {}".format(name))
        print("PESQUERIA  : {}".format(pesqueria))
        print("ARTE DE PESCA  : {}".format(arteDePesca))
        print("KG     : {}".format(kg))
        print("PRESENTACION     : {}".format(presentacion))
        print("ZONA DE CAPTURA:     : {}".format(zonaDeCaptura))
        print("FECHA  : {}".format(fecha))
        print("EMBARCACION  : {}".format(embarcacion))
        print("")

    else:
        raise MarinaException("Folder not found: {}".format(name))


def do_create(args):

    name = args.name
    pesqueria = args.pesqueria
    arteDePesca = args.arteDePesca
    kg = args.kg
    presentacion = args.presentacion
    zonaDeCaptura = args.zonaDeCaptura
    fecha = args.fecha
    embarcacion = args.embarcacion

    #print("do_create: " +  name)
    
    url = _get_url(args)
    keyfile = _get_keyfile(args)
    auth_user, auth_password = _get_auth_info(args)

    client = MarinaClient(base_url=url, keyfile=keyfile)

    if args.wait and args.wait > 0:
        response = client.create(
            name, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion,
            wait=args.wait,
            auth_user=auth_user,
            auth_password=auth_password)
    else:
        response = client.create(
            name, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion,
            auth_user=auth_user,
            auth_password=auth_password)

    #print("do_create - Response: {}".format(response))


def do_update(args):
    name = args.name
    pesqueria = args.pesqueria
    arteDePesca = args.arteDePesca
    kg = args.kg
    presentacion = args.presentacion
    zonaDeCaptura = args.zonaDeCaptura
    fecha = args.fecha
    embarcacion = args.embarcacion

    url = _get_url(args)
    keyfile = _get_keyfile(args)
    auth_user, auth_password = _get_auth_info(args)

    client = MarinaClient(base_url=url, keyfile=keyfile)

    if args.wait and args.wait > 0:
        response = client.update(
            name, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion, wait=args.wait,
            auth_user=auth_user,
            auth_password=auth_password)
    else:
        response = client.update(
            name, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion,
            auth_user=auth_user,
            auth_password=auth_password)

    #print("do_update - Response: {}".format(response))


def do_delete(args):
    name = args.name

    url = _get_url(args)
    keyfile = _get_keyfile(args)
    auth_user, auth_password = _get_auth_info(args)

    client = MarinaClient(base_url=url, keyfile=keyfile)

    if args.wait and args.wait > 0:
        response = client.delete(
            name, wait=args.wait,
            auth_user=auth_user,
            auth_password=auth_password)
    else:
        response = client.delete(
            name, auth_user=auth_user,
            auth_password=auth_password)

    #print("do_delete - Response: {}".format(response))


def _get_url(args):
    return DEFAULT_URL if args.url is None else args.url


def _get_keyfile(args):
    username = getpass.getuser() if args.username is None else args.username
    home = os.path.expanduser("~")
    key_dir = os.path.join(home, ".sawtooth", "keys")

    return '{}/{}.priv'.format(key_dir, username)


def _get_auth_info(args):
    auth_user = args.auth_user
    auth_password = args.auth_password
    if auth_user is not None and auth_password is None:
        auth_password = getpass.getpass(prompt="Auth Password: ")

    return auth_user, auth_password


def main(prog_name=os.path.basename(sys.argv[0]), args=None):
    if args is None:
        args = sys.argv[1:]
    parser = create_parser(prog_name)
    args = parser.parse_args(args)

    if args.verbose is None:
        verbose_level = 0
    else:
        verbose_level = args.verbose

    setup_loggers(verbose_level=verbose_level)

    if args.command == 'create':
        do_create(args)
    elif args.command == 'list':
        do_list(args)
    elif args.command == 'show':
        do_show(args)
    elif args.command == 'update':
        do_update(args)
    elif args.command == 'delete':
        do_delete(args)
    else:
        raise MarinaException("invalid command: {}".format(args.command))


def main_wrapper():
    try:
        main()
    except MarinaException as err:
        print("Error: {}".format(err), file=sys.stderr)
        sys.exit(1)
    except KeyboardInterrupt:
        pass
    except SystemExit as err:
        raise err
    except BaseException as err:
        traceback.print_exc(file=sys.stderr)
        sys.exit(1)
