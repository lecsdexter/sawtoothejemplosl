import hashlib

from sawtooth_sdk.processor.exceptions import InternalError

import logging

MARINA_NAMESPACE = hashlib.sha512('marina'.encode("utf-8")).hexdigest()[0:6]
LOGGER = logging.getLogger(__name__)

def _make_marina_address(name):
    return MARINA_NAMESPACE + \
        hashlib.sha512(name.encode('utf-8')).hexdigest()[:64]

class Trazabilidad(object):
    def __init__(self, nombreDeLaCarpeta, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion):
        self.nombreDeLaCarpeta = nombreDeLaCarpeta
        self.pesqueria = pesqueria
        self.arteDePesca = arteDePesca
        self.kg = kg
        self.presentacion = presentacion
        self.zonaDeCaptura = zonaDeCaptura
        self.fecha = fecha
        self.embarcacion = embarcacion

class MarinaState(object):

    TIMEOUT = 3

    def __init__(self,context):
        """Constructor.

        Args:
            context (sawtooth_sdk.processor.context.Context): Access to
                validator state from within the transaction processor.
        """

        self._context = context
        self._address_cache = {}

    def borrar_trazabilidad(self, nombre_carpeta):
        """Delete the trazabilidad named nombre_carpeta from state.

        Args:
            nombre_carpeta (str): The name.

        Raises:
            KeyError: The trazabilidad with nombre_carpeta does not exist.
        """        
        carpetas = self._cargar_carpetas(nombre_carpeta=nombre_carpeta)

        del carpetas[nombre_carpeta]
        if carpetas:
            self._guardar_carpeta(nombre_carpeta, carpetas=carpetas)
        else:
            self._borrar_carpeta(nombre_carpeta)        

    def set_trazabilidad(self, nombre_carpeta, trazabilidad):
        """Store the trazabilidad in the validator state.

        Args:
            nombre_carpeta (str): The carpeta.
            trazabilidad (Trazabilidad): The information specifying the current trazabilidad.
        """
        
        carpetas = self._cargar_carpetas(nombre_carpeta = nombre_carpeta)
        #LOGGER.debug("2.- set_trazabilidad - carpetas: " + str(carpetas))

        carpetas[nombre_carpeta] = trazabilidad
        #LOGGER.debug("2.- set_trazabilidad - carpetas[nombre_carpeta]: " + str(carpetas[nombre_carpeta]))

        self._guardar_carpeta(nombre_carpeta, carpetas = carpetas)

    def get_trazabilidad(self, nombre_carpeta):
        """Get the trazabilidad associated with nombre_carpeta.

        Args:
            nombre_carpeta (str): The name.

        Returns:
            (Trazabilidad): All the information specifying a trazabilidad.
        """
        carpetas_cargadas = self._cargar_carpetas(nombre_carpeta=nombre_carpeta).get(nombre_carpeta)
        #LOGGER.debug("1.- get_trazabilidad - carpetas_cargadas: " + str(carpetas_cargadas))
        return carpetas_cargadas

    def _guardar_carpeta(self, nombre_carpeta, carpetas):
        #LOGGER.debug("a.- _guardar_carpeta - carpetas: " + str(carpetas))
        address = _make_marina_address(nombre_carpeta)        
        #LOGGER.debug("b.- _guardar_carpeta - address: " + str(address))
        state_data = self._serialize(carpetas)
        self._address_cache[address] = state_data        
        #LOGGER.debug("c.- _guardar_carpeta - state_data: " + str(state_data))
        

        self._context.set_state(
            {address: state_data},
            timeout = self.TIMEOUT
        )


    def _borrar_carpeta(self, nombre_carpeta):
        address = _make_marina_address(nombre_carpeta)

        self._context.delete_state(
            [address],
            timeout = self.TIMEOUT
        )

        self._address_cache[address] = None

    
    def _cargar_carpetas(self, nombre_carpeta):
        address = _make_marina_address(nombre_carpeta)

        if address in self._address_cache:
            if self._address_cache[address]:
                serialized_folders = self._address_cache[address]
                folders = self._deserialize(serialized_folders)
            else:
                folders = {}
        else:
            state_entries = self._context.get_state(
                [address],
                timeout=self.TIMEOUT
            )
            #LOGGER.debug("a.- _cargar_carpetas - state_entries: " + str(state_entries))
            #LOGGER.debug("a.- _cargar_carpetas - state_entries[0].data: " + str(state_entries[0].data))
            if state_entries: #L: si se sincronizo nuevo folder desde otros nodos...
                #LOGGER.debug("a.- _cargar_carpetas - self._address_cache[address]: " + str(self._address_cache[address]))
                self._address_cache[address] = state_entries[0].data
                #LOGGER.debug("a.- _cargar_carpetas - self._address_cache[address]: " + str(self._address_cache[address]))
                folders = self._deserialize(data=state_entries[0].data)
            else:
                self._address_cache[address] = None
                folders = {}

            #LOGGER.debug("b.- _cargar_carpetas - folders: " + str(folders))
        return folders

    
    def _deserialize(self, data):
        """Take bytes stored in state and deserialize them into Python
        Trazabilidad objects.

        Args:
            data (bytes): The UTF-8 encoded string stored in state.

        Returns:
            (dict): folder name (str) keys, Trazabilidad values.
        """  

        folders = {}      
        try:
            for folder in data.decode().split("|"):
                nombreDeLaCarpeta, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion = folder.split(",")

                folders[nombreDeLaCarpeta] = Trazabilidad(nombreDeLaCarpeta, pesqueria, arteDePesca, kg, presentacion, zonaDeCaptura, fecha, embarcacion)
        except ValueError:
            raise InternalError("Failed to deserialize trazabilidad data")

        return folders        

    def _serialize (self, folders):
        """Takes a dict of folder objects and serializes them into bytes.

        Args:
            trazabilidad (dict): folder name (str) keys, Trazabilidad values.

        Returns:
            (bytes): The UTF-8 encoded string stored in state.
        """       
        
        traz_strs = []
        for name, f in folders.items():
            traz_str = ",".join(
                [name, f.pesqueria, f.arteDePesca, f.kg, f.presentacion, f.zonaDeCaptura, f.fecha, f.embarcacion])
            traz_strs.append(traz_str)

        return "|".join(sorted(traz_strs)).encode()     
