from sawtooth_sdk.processor.exceptions import InvalidTransaction

class MarinaPayload(object):

    def __init__(self, payload):
        try:
            # The payload is csv utf-8 encoded string
            folderName, action, fishery, fishingGear, kg, presentation, captureZone, date, embarkation = payload.decode().split(",")
        except ValueError:
            raise InvalidTransaction("Invalid payload serialization - " + ValueError.message)  


        if not folderName:
            raise InvalidTransaction('Folder name is required')

        if action not in ('create', 'update', 'delete'):
            raise InvalidTransaction('Invalid action: {}'.format(action))

        self._folderName = folderName
        self._action = action
        self._fishery = fishery        
        self._fishingGear = fishingGear
        self._kg = kg
        self._presentation = presentation
        self._captureZone = captureZone
        self._date = date
        self._embarkation = embarkation


    @staticmethod
    def from_bytes(payload):
        return MarinaPayload(payload=payload)

    @property
    def folderName(self):
        return self._folderName

    @property
    def action(self):
        return self._action

    @property
    def fishery(self):
        return self._fishery

    @property
    def fishingGear(self):
        return self._fishingGear

    @property
    def kg(self):
        return self._kg

    @property
    def presentation(self):
        return self._presentation

    @property
    def captureZone(self):
        return self._captureZone

    @property
    def date(self):
        return self._date

    @property
    def embarkation(self):
        return self._embarkation
